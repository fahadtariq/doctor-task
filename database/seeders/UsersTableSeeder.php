<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'John Doe',
                'email' => 'johndoe@example.com',
                'password' => bcrypt('12345678@'),
                'timezone' => 'PKT',
                'role' => 'doctor'
            ],
            [
                'name' => 'Jane Doe',
                'email' => 'janedoe@example.com',
                'password' => bcrypt('12345678@'),
                'timezone' => 'IST',
                'role' => 'doctor'
            ],
            [
                'name' => 'Bob Smith',
                'email' => 'bobsmith@example.com',
                'password' => bcrypt('12345678@'),
                'timezone' => 'CST',
                'role' => 'patient'
            ],
            [
                'name' => 'Micheal Harry',
                'email' => 'mikeharry@example.com',
                'password' => bcrypt('12345678@'),
                'timezone' => 'GST',
                'role' => 'patient'
            ]
        ];
    
        foreach ($users as $userData) {
            DB::table('users')->insert($userData);
        }
    }
}
