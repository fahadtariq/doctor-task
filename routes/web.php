<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\DoctorController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\AppointmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/', function(){
    return redirect('/login');
});

Auth::routes();
Route::get('/doctors/slots', [DoctorController::class, 'mySlot'])->name('doctor.myslot');
Route::get('/doctors/block/{id}', [DoctorController::class, 'blockSlot'])->name('doctor.blockslot');
Route::get('/doctors/unblock/{id}', [DoctorController::class, 'unblockSlot'])->name('doctor.unblockslot');

Route::resource('/doctors', DoctorController::class);

Route::get('/doctor/generate-slot', [HomeController::class, 'listOfSlot']);
Route::get('/doctor/get-slot/{doctor_id}', [HomeController::class, 'getSlots']);
Route::get('/home', [HomeController::class, 'index'])->name('home');
// Book Appointment
Route::post('/book', [AppointmentController::class, 'store'])->name('booking.store');
Route::post('/reschedule', [AppointmentController::class, 'reschedule'])->name('booking.reschedule');
Route::get('/doctor/appointments', [AppointmentController::class, 'index'])->name('doctor.appointment');
Route::get('/patient/appointments', [AppointmentController::class, 'patientAppointment'])->name('patient.appointment');
