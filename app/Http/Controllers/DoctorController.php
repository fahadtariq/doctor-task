<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\DoctorSlot;
use App\Models\User;
use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class DoctorController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = User::where(["role"=>"doctor"])->get();
        if(Auth::user()->role == "patient"){
            return view('welcome',get_defined_vars());
        }else{
            return redirect('doctor/appointments');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $auth = Auth::user();
        if($auth->role == "patient"){
            $doctorSlotStarttime = [];
            $doctorSlotEndtime = [];
            $doctorBookedSlotStarttime = [];
            $doctorBookedSlotEndtime = [];

            $user = User::find($id);
            $slots = DoctorSlot::where([
                        'doctor_id'=>$id,
                        'status'=>'available'
                    ])->get();
            $bookings = Booking::where('doctor_id',$id)->get();
            
            $timezone = $user->timezone;

            // iterate for start and end time with specific format of booking
            if(count($bookings)>0){
                foreach ($bookings as $slot) {
                    $startTime = Carbon::parse($slot->start_time);
                    $endTime = Carbon::parse($slot->end_time);
                    $doctorBookedSlotStarttime[] = $startTime->format('g:i A');
                    $doctorBookedSlotEndtime[] = $endTime->format('g:i A');
                }
            }

            // iterate for start and end time with specific format
            if(count($slots)>0){
                foreach ($slots as $slot) {
                    $startTime = Carbon::parse($slot->start_time)->timezone($timezone);
                    $endTime = Carbon::parse($slot->end_time)->timezone($timezone);
                    $doctorSlotStarttime[] = $startTime->format('g:i A');
                    $doctorSlotEndtime[] = $endTime->format('g:i A');
                }
            }

            // Remove the booked time slots from the list of all time slots
            if(count($slots)>0 && count($bookings)>0){
                // Remove start time booked slot
                $doctorSlotStarttime = array_diff($doctorSlotStarttime, $doctorBookedSlotStarttime);
                // Remove end time booked slot
                $doctorSlotEndtime = array_diff($doctorSlotEndtime, $doctorBookedSlotEndtime);
            }

            return view('doctordetail',get_defined_vars());
        }else{
            return redirect("/");
        }
    }

    public function mySlot()
    {
        $doctorSlot = DoctorSlot::where("doctor_id",Auth::id())->get();
        return view("doctorslot",get_defined_vars());
    }

    public function blockSlot($id)
    {
        DoctorSlot::find($id)->update(["status"=>"blocked"]);
        return redirect()->back()->with("success","Slot Blocked Successfully");
    }

    public function unblockSlot($id)
    {
        DoctorSlot::find($id)->update(["status"=>"available"]);
        return redirect()->back()->with("success","Slot Unblock Successfully");
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
