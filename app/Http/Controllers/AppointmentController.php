<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\User;
use Auth;
use Carbon\Carbon;

class AppointmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::where("id",Auth::id())->with("bookings")->first();
        $bookingUser = Booking::where("doctor_id",Auth::id())->with('patient')->get();
        foreach ($bookingUser as $appointment) {
            // Save start_time according to timezone
            $startTime = Carbon::createFromFormat('H:i:s', $appointment->start_time);
            $startTime->timezone($user->timezone);
            $appointment->start_time = $startTime->format('H:i:s');
            // Save start_time according to timezone
            $endTime = Carbon::createFromFormat('H:i:s', $appointment->end_time);
            $endTime->timezone($user->timezone);
            $appointment->end_time = $endTime->format('H:i:s');
        }
        return view("doctorappointment",get_defined_vars());
    }

    public function patientAppointment()
    {
        $auth = Auth::user();
        if($auth->role == "patient"){
            $user = User::where("id",Auth::id())->with("patientBookings")->first();
            $bookingUser = Booking::where("patient_id",Auth::id())->with('doctor')->get();
            return view("patientappointment",get_defined_vars());
        }else{
            return redirect()->back();
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = User::find($request->doctor_id);
        // Generate start and end time according
        $startTime = Carbon::createFromFormat('g:i A', $request->start_time);
        $endTime = Carbon::createFromFormat('g:i A', $request->end_time);
        // Create booking
        Booking::create([
            "start_time" => $startTime->format("H:i:s"),
            "end_time" => $endTime->format("H:i:s"),
            "patient_id" => Auth::id(),
            "doctor_id" => $request->doctor_id
        ]);
        return redirect()->back()->with("success","Appointment Booked Successfully");
    }
    // Function for reschedule booking
    public function reschedule(Request $request)
    {
        // Create booking for reschedulr
        Booking::create([
            "start_time" => $request->start_time,
            "end_time" => $request->end_time,
            "patient_id" => Auth::id(),
            "doctor_id" => $request->doctor_id,
            "status" => "reschedule"
        ]);
        return redirect()->back()->with("success","Appointment Reschedule Successfully");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
