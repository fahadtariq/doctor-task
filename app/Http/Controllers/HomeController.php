<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\DoctorSlot;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $doctors = User::where(["role"=>"doctor"])->get();
        // dd(Auth::user()->role);
        if(Auth::user()->role == "patient"){
            return view('welcome',get_defined_vars());
        }else{
            return redirect('doctor/appointments');
        }
    }

    /**
     * Generate a listing of the slot of doctor.
     *
     * @return \Illuminate\Http\Response
     */
    public function listOfSlot()
    {
        // Get user with role doctor
        $doctor1 = User::where(["id"=>1,"role"=>"doctor"])->first();
        $doctor2 = User::where(["id"=>2,"role"=>"doctor"])->first();
        // Get time slot of doctor 1
        $startTime1 = "09:00 AM";
        $endTime1 = "03:00 PM";
        // Get time slot of doctor 2
        $startTime2 = "03:00 PM";
        $endTime2 = "09:00 PM";
        // Generate slot of doctors
        $doctorSlot1 = $doctor1->generateSlot($startTime1,$endTime1,$doctor1->id);
        $doctorSlot2 = $doctor2->generateSlot($startTime2,$endTime2,$doctor2->id);
        // Insert slot in doctor_slots table
        DoctorSlot::insert($doctorSlot1);
        DoctorSlot::insert($doctorSlot2);
        // response
        return redirect('/')->with("success","Slots generated");
    }
    
    // Function for get slots according to timezone
    public function getSlots($doctor_id)
    {
        $user = User::find($doctor_id);
        if($user){
            $slots = DoctorSlot::where('doctor_id',$doctor_id)->get();
            $timezone = $user->timezone;
            if(count($slots)>0){
                foreach ($slots as $slot) {
                    $startTime = Carbon::parse($slot->start_time)->timezone($timezone);
                    $endTime = Carbon::parse($slot->end_time)->timezone($timezone);
        
                    echo " ".$startTime->format('g:i A') . ' - ' . $endTime->format('g:i A');
                }
            }else{
                echo "No slots generated against this doctor";
            }
        }else{
            echo "No doctor found against this id";
        }
    }
}
