<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'email',
        'timezone',
        'password',
        'role'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    // Generate slot by passing parameter start time and end time
    public function generateSlot(string $startTime, string $endTime, $doctorId)
    {
        $startTime = Carbon::createFromFormat('g:i A', $startTime);
        $endTime = Carbon::createFromFormat('g:i A', $endTime);
        
        $slots = [];
        $interval = Carbon::MINUTES_PER_HOUR / 2;

        for ($time = $startTime; $time->lt($endTime); $time->addMinutes($interval)) {
            $slots[] = [
                'start_time' => $time->format('H:i:s'),
                'end_time' => $time->copy()->addMinutes($interval)->format('H:i:s'),
                'doctor_id' => $doctorId,
                'status' => "available"
            ];
        }

        return $slots;
    }

    public function bookings()
    {
        return $this->hasMany(Booking::class,"doctor_id","id");
    }

    public function patientBookings()
    {
        return $this->hasMany(Booking::class,"patient_id","id");
    }
}
