<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;
    protected $guarded = [];
    public function patient()
    {
        return $this->hasOne(User::class,"id","patient_id");
    }
    public function doctor()
    {
        return $this->hasOne(User::class,"id","doctor_id");
    }
}
